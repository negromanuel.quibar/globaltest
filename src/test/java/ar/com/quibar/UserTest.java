package ar.com.quibar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.com.quibar.user.model.User;
import ar.com.quibar.user.repository.UserRepository;
import ar.com.quibar.user.service.UserService;

@SpringBootTest
class UserTest {

	UserRepository repository = mock(UserRepository.class);
	
	@Autowired
	UserService service = new UserService(repository);
	
	@Test
	void saveUserTest() {
		final User user = new User();
		
		when(repository.save(any(User.class))).thenReturn(user);
		
		User responseUser = service.save(user);
		
		assertEquals(user, responseUser);
	}

}
