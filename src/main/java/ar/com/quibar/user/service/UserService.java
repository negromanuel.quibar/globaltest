package ar.com.quibar.user.service;

import org.springframework.stereotype.Service;

import ar.com.quibar.shared.exceptions.ConflictResourceException;
import ar.com.quibar.user.model.User;
import ar.com.quibar.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

	private final UserRepository repository;
	
	public User save(User user) {
		if (repository.findByEmail(user.getEmail()).isPresent() ) {
			log.error("Email {} existe en el sistema", user.getEmail());
			throw new ConflictResourceException("Email ya existe en el sistema!");
		}
		return repository.save(user);
	}
	
}
