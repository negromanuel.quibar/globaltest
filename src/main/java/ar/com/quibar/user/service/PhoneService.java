package ar.com.quibar.user.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ar.com.quibar.user.model.Phone;
import ar.com.quibar.user.repository.PhoneRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PhoneService {

	private final PhoneRepository repository;
	
	public List<Phone> save(List<Phone> phones) {
		return repository.saveAll(phones);
	}
}
