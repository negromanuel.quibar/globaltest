package ar.com.quibar.user.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.quibar.user.model.Phone;

@Repository
public interface PhoneRepository extends JpaRepository<Phone, UUID> {

}
