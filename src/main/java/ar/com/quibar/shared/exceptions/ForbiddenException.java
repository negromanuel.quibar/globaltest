package ar.com.quibar.shared.exceptions;

import static org.springframework.http.HttpStatus.FORBIDDEN;

import org.springframework.web.server.ResponseStatusException;

public class ForbiddenException extends ResponseStatusException {

	private static final long serialVersionUID = 1L;

	public ForbiddenException() {
        super(FORBIDDEN);
    }

    public ForbiddenException(String message) {
        super(FORBIDDEN,message);
    }

    public ForbiddenException(String message, Throwable cause) {
        super(FORBIDDEN, message, cause);
    }

}
