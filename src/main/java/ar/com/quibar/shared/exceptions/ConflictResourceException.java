package ar.com.quibar.shared.exceptions;

import static org.springframework.http.HttpStatus.CONFLICT;

import org.springframework.web.server.ResponseStatusException;


public class ConflictResourceException extends ResponseStatusException {

	private static final long serialVersionUID = 3041012600648969565L;

	public ConflictResourceException(String message) {
        super(CONFLICT, message);
    }

    public ConflictResourceException(String message, Throwable cause) {
        super(CONFLICT, message, cause);
    }

    public ConflictResourceException(Throwable cause) {
        super(CONFLICT, null, cause);
    }
}
