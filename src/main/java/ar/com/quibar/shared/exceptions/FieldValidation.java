package ar.com.quibar.shared.exceptions;

import org.springframework.validation.FieldError;

import lombok.Data;

@Data
public class FieldValidation {
	private String name;
	private String msg;

	public FieldValidation(FieldError fieldError) {
		this.name = fieldError.getField();
		this.msg = fieldError.getDefaultMessage();
	}
}
