package ar.com.quibar.shared.exceptions;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.server.ResponseStatusException;


public class InvalidResourceException extends ResponseStatusException {

	private static final long serialVersionUID = 7880830570734309110L;

	public InvalidResourceException()
    {
        super(BAD_REQUEST);
    }

    public InvalidResourceException(String message)
    {
        super(BAD_REQUEST, message);
    }
    
    public InvalidResourceException(String message, Throwable cause) {
        super(BAD_REQUEST, message, cause);
    }
}
