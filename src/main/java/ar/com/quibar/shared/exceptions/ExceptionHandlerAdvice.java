package ar.com.quibar.shared.exceptions;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler {
	
	public static final String CODE = "code";
	public static final String MESSAGE = "message";
	public static final String STATUS = "status";
	public static final String FIELDS = "fields";
	
	@ExceptionHandler(Exception.class)
	ResponseEntity<Object> handleException(Exception ex, Object body, WebRequest request) {
		log.error(ex.getMessage(), ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}
	
	@Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Map<String, Object> body = new LinkedHashMap<>();
		body.put(CODE, "-1");
		body.put(STATUS, status);
		body.put(MESSAGE, ex.getMessage());
		
		return new ResponseEntity<>(body, headers, status);
	}
	
	@ExceptionHandler(ResponseStatusException.class)
	public final ResponseEntity<ExceptionResponse> handleValidatoinException(ResponseStatusException ex, WebRequest request) {

		final ExceptionResponse response = ExceptionResponse.builder()
				.message(ex.getMessage())
				.dateTime(LocalDateTime.now())
				.build();

		return new ResponseEntity<>(response, ex.getStatus());
	}
	
	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		Map<String, Object> body = new LinkedHashMap<>();
		body.put(CODE, -1);
		body.put(MESSAGE, "campos invalidos");
		
		List<FieldValidation> fields = ex.getBindingResult()
				.getFieldErrors()
				.stream()
				.map(FieldValidation::new)
				.collect(Collectors.toList());

		body.put(FIELDS, fields);
		log.info("MethodArgumentNotValidException: {}", body);
		return new ResponseEntity<>(body, headers, status);
	}
	
}
