package ar.com.quibar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GlobaltestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlobaltestApplication.class, args);
	}

}
