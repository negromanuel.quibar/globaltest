package ar.com.quibar.security.controller;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ar.com.quibar.security.dto.SignUpRequest;
import ar.com.quibar.security.dto.SignUpResponse;
import ar.com.quibar.security.service.SignUpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Controller
@RestController
@RequiredArgsConstructor
@Slf4j
public class AuthController {
	
	private final SignUpService service;
	
	
	@PostMapping(value = "/auth/signup", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<SignUpResponse> login(@Valid @RequestBody SignUpRequest request){
		log.info("/auth/signup, body = {}", request);
		return service.signUp(request);
	}

}
