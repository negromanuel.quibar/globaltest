package ar.com.quibar.security.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {

	@NotBlank(message = "Campo Obligatorio")
	private String name;
	
	@NotBlank(message = "Campo Obligatorio")
	@Email(message = "Formato de correo invalido")
	private String email;
	
	@NotBlank(message = "Campo Obligatorio")
	@Pattern(regexp = "^(?=\\w*\\d){2,}(?=\\w*[A-Z]){1,}(?=\\w*[a-z])\\S{6,}$", 
		message = "La contraseña debe tener una longitud minima de 8 caraceres y contener al menos 2 numeros, y una mayuscula")
	private String password;
	
	@Valid
	private List<PhoneDto> phones;
	
}
