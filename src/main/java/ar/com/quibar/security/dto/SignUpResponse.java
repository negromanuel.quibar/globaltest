package ar.com.quibar.security.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignUpResponse {

	private UUID id;
	
	private String name;
	
	private String email;
	
	private String password;
	
	private LocalDateTime created;
	
	private LocalDateTime modified;
	
	private LocalDateTime lastLogin;
	
	private String token;
	
	private Boolean isActive;
	
	private List<PhoneDto> phones;
	
}
