package ar.com.quibar.security.dto;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhoneDto{
	
	private UUID id;
	
	@NotBlank(message = "Campo Obligatorio")
	private String number;
	
	@NotNull(message = "Campo Obligatorio")
	private Short citycode;
	
	@NotNull(message = "Campo Obligatorio")
	private Short contrycode;
}
