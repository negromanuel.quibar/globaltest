package ar.com.quibar.security.utils;

import java.io.Serializable;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ar.com.quibar.user.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@Component
@Scope("prototype")
public class JwtTokenUtil implements Serializable {

	private static final long serialVersionUID = 2338263471043309378L;

	@Value("${springboot.jjwt.secret}")
	private String secret;

	@Value("${springboot.jjwt.expiration}")
	private String expiration;

	public String generateToken(String email) {
		Instant expirationTime = Instant.now().plus(Long.parseLong(expiration), ChronoUnit.MINUTES);
		Date expirationDate = Date.from(expirationTime);

		Key key = Keys.hmacShaKeyFor(secret.getBytes());

		String compactTokenString = Jwts.builder()
				.setSubject(email)
				.setExpiration(expirationDate)
				.setIssuedAt(new Date())
				.signWith(key, SignatureAlgorithm.HS512).compact();
		return "Bearer " + compactTokenString;
	}

	public User decodeUser(String token) {
		Jws<Claims> jwsClaims = Jwts.parserBuilder()
				.setSigningKey(secret.getBytes())
				.build()
				.parseClaimsJws(token);
		String username = jwsClaims.getBody().getSubject();
		return User.builder().name(username).build();
	}

}
