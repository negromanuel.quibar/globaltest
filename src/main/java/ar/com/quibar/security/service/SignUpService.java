package ar.com.quibar.security.service;

import static org.springframework.http.ResponseEntity.ok;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ar.com.quibar.security.dto.PhoneDto;
import ar.com.quibar.security.dto.SignUpRequest;
import ar.com.quibar.security.dto.SignUpResponse;
import ar.com.quibar.security.utils.JwtTokenUtil;
import ar.com.quibar.user.model.Phone;
import ar.com.quibar.user.model.User;
import ar.com.quibar.user.service.PhoneService;
import ar.com.quibar.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class SignUpService {

	private final UserService userService;
	private final PhoneService phoneService;
	private final PasswordEncoder passwordEncoder;
	private final JwtTokenUtil jwtTokenUtil;
	
	@Transactional
	public ResponseEntity<SignUpResponse> signUp(SignUpRequest request) {
		String pass = passwordEncoder.encode(request.getPassword());
		String token = jwtTokenUtil.generateToken(request.getEmail());
		LocalDateTime now = LocalDateTime.now();
		final User user = User.builder()
				.name(request.getName())
				.email(request.getName())
				.password(pass)
				.token(token)
				.created(now)
				.modified(now)
				.lastLogin(now)
				.isActive(true)
				.build();
		userService.save(user);
		log.info("Usuario guardado con exito, id: {}", user.getId());
		List<Phone> phones =request.getPhones().stream()
			.map(phoneRequest -> Phone.builder()
					.citycode(phoneRequest.getCitycode())
					.contrycode(phoneRequest.getContrycode())
					.number(phoneRequest.getNumber())
					.user(user)
					.build())
			.collect(Collectors.toList());
		
		phoneService.save(phones);
		log.info("{} telefono(s) guardado con exito", phones.size());
		
		SignUpResponse response = new SignUpResponse();
		BeanUtils.copyProperties(user,response);
		List<PhoneDto> phonesDto = phones.stream()
				.map(phone -> {
					PhoneDto phoneDto = new PhoneDto();
					BeanUtils.copyProperties(phone, phoneDto);
					return phoneDto;
				}).collect(Collectors.toList());
		response.setPhones(phonesDto);
				
		return ok(response);
	}
}
